// This document contains all the endpoints for our application (also http methods)

const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

router.get("/viewTasks", (req, res)=>{
									//returns result from our controller
	taskController.getAllTask().then(result => {
		res.send(result)
	})
})

router.post("/addNewTask" , (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
});
						//:id will serve as our wildcard
router.delete("/deleteTask/:id" , (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
});

router.put("/updateTask/:id" , (req, res) => {
							//(req.params.id) will be the basis of what doc we will update
							//(req.body) doc/contents as replacement
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
});


// Activity S36 ---------------------------------


router.get("/specificTask/:id", (req, res)=>{
	taskController.getTask(req.params.id).then(result => {
		res.send(result)
	})
});

router.put("/:id/complete" , (req, res) => {
	taskController.updateTaskStatus(req.params.id, req.body).then(result => res.send(result));
});





module.exports = router;

