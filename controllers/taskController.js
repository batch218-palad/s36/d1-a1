//This documents contains our app feature in displaying

const Task = require("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error)=>{
		if(error){
			console.log(error);
			return false; // "Error detected"
		}
		else{
			return task;
		}
	})
};

//"taskId" parameter will serve as storage of id from our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
		if(err){
			console.log(err);
			return false; // "Error detected"
		}
		else{
			return removedTask;
		}
	})
};

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(error);
			return false; // "Error detected"
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
		
			else{
			return updateTask;
			}
		})
	})
};




// Activity S36 ---------------------------------



module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((oneTask, err)=>{
		if(err){
			console.log(err);
			return false; // "Error detected"
		}
		else{
			return oneTask;
		}
	})
};



module.exports.updateTaskStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(error);
			return false; // "Error detected"
		}
		result.status = newStatus.status;

		return result.save().then((updateTaskStatus, saveErr)=>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
		
			else{
			return updateTaskStatus;
			}
		})
	})
};














