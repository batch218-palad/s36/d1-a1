/*
	TERMINAL
		npm init -y
		npm i express
		npm i mongoose
		touch .gitignore >> node_modules (inside)
*/

const express = require("express");
const mongoose = require("mongoose");

/*
models folder >> task.js
controllers folder >> taskController.js
routes folder >> taskRoute.js
*/

const taskRoute = require("./routes/taskRoute.js");

// Server Setup
const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// DB Connection
mongoose.connect("mongodb+srv://admin:admin@batch218-to-do.j232gim.mongodb.net/toDo?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));

// terminal:
// nodemon index.js


// Activity S36 ---------------------------------


/*
[GET -Wildcard required]
1. Create a controller function for retrieving a specific task.
2. Create a route 
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

[PUT - Update]
5. Create a controller function for changing the status of a task to "complete".
6. Create a route
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.
*/













